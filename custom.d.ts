declare module '@strapi/design-system/*';
declare module '@strapi/design-system';
declare module '@strapi/icons';
declare module '@strapi/icons/*';
declare module '@strapi/helper-plugin';

declare module '@strapi/strapi' {
  import {GraphQLExtension} from "./server";

  interface Strapi {
    createGraphQLExtension: (factory: (strapi: Strapi) => GraphQLExtension) => GraphQLExtension
  }
}
