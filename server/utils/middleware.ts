import 'nexus';
import { GraphQLResolveInfo } from 'graphql';
import { plugin } from 'nexus';
import { MiddlewareFn } from 'nexus/dist-esm/plugin';
import { ResolveContext } from './types';
import { printedGenTyping } from 'nexus/dist/utils';


/**
 * The Resolver type represents a GraphQL resolver function.
 */
export type Resolver = <Parent extends Object, Args extends Object, Context = ResolveContext>(
  parent: Parent,
  args: Args,
  context: Context,
  info: GraphQLResolveInfo
) => any | Promise<any>;

/**
 * Middleware is a function that takes a resolver and returns a new resolver.
 */
export type Middleware = (resolver: Resolver) => Resolver;

const fieldDefTypes = printedGenTyping({
  optional: true,
  name: 'middlewares',
  description: `
    Authorization for an individual field. Returning "true"
    or "Promise<true>" means the field can be accessed.
    Returning "false" or "Promise<false>" will respond
    with a "Not Authorized" error for the field.
    Returning or throwing an error will also prevent the
    resolver from executing.
  `,
  type: 'any',
})

export const nexusMiddlewarePlugin = () => plugin({
  name: 'NexusMiddlewarePlugin',
  description: 'The middleware plugin allows attaching middleware functions to individual fields.',

  /**
   * Defines the field definition types for the plugin.
   */
  fieldDefTypes: fieldDefTypes,

  /**
   * Hook into field resolver creation to apply middleware.
   */
  onCreateFieldResolver(config): MiddlewareFn | undefined {
    const { fieldConfig } = config;

    // @ts-ignore
    const middlewares: Middleware[] = fieldConfig.extensions?.middlewares || [];

    if (middlewares.length === 0) {
      return undefined; // No middleware to apply
    }

    // Convert Middleware[] to MiddlewareFn[]
    const middlewareFns: MiddlewareFn[] = middlewares.map(middleware => {
      return async (source, args, context, info, next) => {
        const newResolver = middleware(next as Resolver);
        return newResolver(source, args, context, info);
      };
    });

    // Compose the middleware functions into a single MiddlewareFn
    return async (source, args, context, info, next) => {
      let index = -1;

      const dispatch = async (i: number): Promise<any> => {
        if (i <= index) throw new Error('next() called multiple times');
        index = i;
        const middlewareFn = middlewareFns[i];
        if (middlewareFn) {
          return middlewareFn(source, args, context, info, () => dispatch(i + 1));
        } else {
          return next(source, args, context, info);
        }
      };

      return dispatch(0);
    };
  },


})


/**
 * Logging Middleware
 * Logs the execution of the resolver.
 */
export const loggingMiddleware: Middleware = (resolver) => {
  return async (parent, args, context, info) => {
    console.log(`Resolving field ${info.parentType.name}.${info.fieldName}`);
    const result = await resolver(parent, args, context, info);
    console.log(`Resolved field ${info.parentType.name}.${info.fieldName} with result:`, result);
    return result;
  };
};
