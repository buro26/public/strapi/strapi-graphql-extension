import type {GraphQLResolveInfo} from "graphql";

type User = {
  id: number
  username: string
  email: string
  provider: string
  password: string
  resetPasswordToken: string|null
  confirmationToken: string|null
  confirmed: boolean
  blocked: boolean
  createdAt: string
  updatedAt: string
  role: {
    id: number
    name: string
    description: string
    type: string
    createdAt: string
    updatedAt: string
  }
}

export type ResolveContext<UserType = User> = {
  state: {
    route: {
      info: {
        type: string
      }
    }
    user: UserType
    isAuthenticated: boolean
    auth: {
      strategy: {
        name: string
        authenticate: any
        verify: any
      }
      credentials: UserType
    }
  }
  koaContext: any
}

export type ResolveArgs<Parent = unknown, Args = unknown, Context = ResolveContext, Info = GraphQLResolveInfo> = {
  parent: Parent
  args: Args
  context: Context
  info: Info
}

export type Entity<T extends Object> = {
  id: string
  attributes: Omit<T, 'id'>
}
