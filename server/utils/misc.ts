export function transformArgs(args: Record<string, any>, { contentType, usePagination }: { contentType: string, usePagination?: boolean }): Record<string, any> {
  const { transformArgs } = strapi.plugin('graphql').service('builders').utils;

  return transformArgs(args, {
    contentType,
    usePagination: usePagination ?? true
  });
}
