/**
 * Easily resolve an entity
 *
 * Based on node_modules/@strapi/plugin-graphql/server/services/builders/type.js
 */

import { Entity, ResolveArgs } from './types';

type ResolveEntityArgs = Omit<ResolveArgs, 'args'> & {
  args: {
    id: string
  }
}

export async function resolveEntity<T extends Object>(contentType: string, resolveArgs: ResolveEntityArgs): Promise<Entity<T> | null> {
  const { transformArgs } = strapi.plugin('graphql').service('builders').utils;
  const { toEntityResponse } = strapi.plugin('graphql').service('format').returnTypes;
  const { buildQueriesResolvers } = strapi.plugin('graphql').service('builders').get('content-api');
  const transformedArgs = transformArgs(resolveArgs.args, { contentType });

  const { findOne } = buildQueriesResolvers({ contentType });

  const item = await findOne(resolveArgs.parent, transformedArgs, resolveArgs.context);

  return toEntityResponse(item, {
    args: resolveArgs.args,
    resourceUID: contentType
  });
}

export const resolveEntityRelation = async <T extends Object>(
  contentType: string,
  relationFieldName: keyof T,
  resolveArgs: ResolveArgs
): Promise<Entity<T>[]> => {
  const { buildAssociationResolver } = strapi.plugin('graphql').service('builders').get('content-api');

  const find = buildAssociationResolver({
    contentTypeUID: contentType,
    attributeName: relationFieldName
  });

  return find(resolveArgs.parent, resolveArgs.args, resolveArgs.context);
};

type EntityResponse<T extends Object> = {
  value: Entity<T> | null
  info: {
    args: Record<string, any>
    resourceUID: string
  }
}

export async function toEntityResponse <T extends Object>(item: T | null, { args, resourceUID }: { args: Record<string, any>, resourceUID: string }): Promise<EntityResponse<T> | null> {
  const { toEntityResponse } = strapi.plugin('graphql').service('format').returnTypes;

  return toEntityResponse(item, {
    args,
    resourceUID
  });
}
