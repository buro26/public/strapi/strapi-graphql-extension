/**
 * Override a field in a content type
 *
 * Based on node_modules/@strapi/plugin-graphql/server/services/builders/type.js
 */


import { constant, isArray, isString, isUndefined } from 'lodash/fp';
import { contentTypes } from '@strapi/utils';
import type { Model } from '@strapi/utils/dist/types';
import type { Schema } from '@strapi/types/dist/types';
import type { GraphQLResolveInfo } from 'graphql';
import type { ResolveContext } from './types';
import type { Middleware } from './middleware';

const getGraphQLService = (name: string) => strapi.plugin('graphql').service(name);

/**
 * Add a scalar attribute to the type definition
 *
 * The attribute is added based on a simple association between a Strapi
 * type and a GraphQL type (the map is defined in `strapiTypeToGraphQLScalar`)
 */
const addScalarAttribute = ({ builder, attributeName, attribute, ...options }: Options) => {
  const { mappers } = getGraphQLService('utils');

  const gqlType = mappers.strapiScalarToGraphQLScalar(attribute.type);

  builder.field(attributeName, { type: gqlType, ...options });
};

/**
 * Add a component attribute to the type definition
 *
 * The attribute is added by fetching the component's type
 * name and using it as the attribute's type
 */
const addComponentAttribute = ({ builder, attributeName, contentType, attribute, ...options }: Options) => {
  let localBuilder = builder;

  const { naming } = getGraphQLService('utils');
  const { getContentTypeArgs } = getGraphQLService('builders').utils;
  const { buildComponentResolver } = getGraphQLService('builders').get('content-api');

  const type = naming.getComponentNameFromAttribute(attribute);

  if (attribute.repeatable) {
    localBuilder = localBuilder.list;
  }

  // @ts-ignore
  const targetComponent = strapi.getModel(attribute.component);

  const resolve = buildComponentResolver({
    contentTypeUID: contentType.uid,
    attributeName,
    strapi,
  });

  const args = getContentTypeArgs(targetComponent, { multiple: !!attribute.repeatable });

  localBuilder.field(attributeName, { type, resolve, args, ...options });
};

/**
 * Add a dynamic zone attribute to the type definition
 *
 * The attribute is added by fetching the dynamic zone's
 * type name and using it as the attribute's type
 */
const addDynamicZoneAttribute = ({ builder, attributeName, contentType, ...options }: Options) => {
  const { naming } = getGraphQLService('utils');
  const { ERROR_CODES } = getGraphQLService('constants');
  const { buildDynamicZoneResolver } = getGraphQLService('builders').get('content-api');

  const { components } = contentType.attributes[attributeName];

  const isEmpty = components.length === 0;
  const type = naming.getDynamicZoneName(contentType, attributeName);

  const resolve = isEmpty
    ? // If the dynamic zone don't have any component, then return an error payload
    constant({
      code: ERROR_CODES.emptyDynamicZone,
      message: `This dynamic zone don't have any component attached to it`,
    })
    : //  Else, return a classic dynamic-zone resolver
    buildDynamicZoneResolver({
      contentTypeUID: contentType.uid,
      attributeName,
    });

  builder.list.field(attributeName, { type, resolve, ...options });
};

/**
 * Add an enum attribute to the type definition
 *
 * The attribute is added by fetching the enum's type
 * name and using it as the attribute's type
 */
const addEnumAttribute = ({ builder, attributeName, contentType, ...options }: Options) => {
  const { naming } = getGraphQLService('utils');

  const type = naming.getEnumName(contentType, attributeName);

  builder.field(attributeName, { type, ...options });
};

/**
 * Add a media attribute to the type definition
 */
const addMediaAttribute = (options: Options) => {
  const { naming } = getGraphQLService('utils');
  const { getContentTypeArgs } = getGraphQLService('builders').utils;
  const { buildAssociationResolver } = getGraphQLService('builders').get('content-api');
  const extension = getGraphQLService('extension');

  const { builder } = options;
  const { attributeName, attribute, contentType } = options;
  const fileUID = 'plugin::upload.file';

  if (extension.shadowCRUD(fileUID).isDisabled()) {
    return;
  }

  const fileContentType = strapi.contentTypes[fileUID];

  const resolve = buildAssociationResolver({
    contentTypeUID: contentType.uid,
    attributeName,
    strapi,
  });

  const args = attribute.multiple ? getContentTypeArgs(fileContentType) : undefined;
  const type = attribute.multiple
    ? naming.getRelationResponseCollectionName(fileContentType)
    : naming.getEntityResponseName(fileContentType);

  builder.field(attributeName, { type, resolve, args, ...options });
};

/**
 * Add a polymorphic relational attribute to the type definition
 */
const addPolymorphicRelationalAttribute = (options: Options) => {
  const { GENERIC_MORPH_TYPENAME } = getGraphQLService('constants');
  const { naming } = getGraphQLService('utils');
  const { buildAssociationResolver } = getGraphQLService('builders').get('content-api');

  let { builder } = options;
  const { attributeName, attribute, contentType } = options;

  const { target } = attribute; // @ts-ignore
  const isToManyRelation = attribute.relation.endsWith('Many');

  if (isToManyRelation) {
    builder = builder.list;
  }

  const resolve = buildAssociationResolver({
    contentTypeUID: contentType.uid,
    attributeName,
    strapi,
  });

  // If there is no specific target specified, then use the GenericMorph type
  if (isUndefined(target)) {
    builder.field(attributeName, {
      type: GENERIC_MORPH_TYPENAME,
      resolve,
      ...options,
    });
  }

  // If the target is an array of string, resolve the associated morph type and use it
  else if (isArray(target) && target.every(isString)) {
    const type = naming.getMorphRelationTypeName(contentType, attributeName);

    builder.field(attributeName, { type, resolve, ...options });
  }
};

/**
 * Add a regular relational attribute to the type definition
 */
const addRegularRelationalAttribute = (options: Options) => {
  const { naming } = getGraphQLService('utils');
  const { getContentTypeArgs } = getGraphQLService('builders').utils;
  const { buildAssociationResolver } = getGraphQLService('builders').get('content-api');
  const extension = getGraphQLService('extension');

  const { builder } = options;
  const { attributeName, attribute, contentType } = options;

  if (extension.shadowCRUD(attribute.target).isDisabled()) {
    return;
  }

  // @ts-ignore
  const isToManyRelation = attribute.relation.endsWith('Many');

  const resolve = buildAssociationResolver({
    contentTypeUID: contentType.uid,
    attributeName,
    strapi,
  });

  // @ts-ignore
  const targetContentType = strapi.getModel(attribute.target);

  const type = isToManyRelation
    ? naming.getRelationResponseCollectionName(targetContentType)
    : naming.getEntityResponseName(targetContentType);

  const args = isToManyRelation ? getContentTypeArgs(targetContentType) : undefined;

  const resolverPath = `${naming.getTypeName(contentType)}.${attributeName}`;
  const resolverScope = `${targetContentType.uid}.find`;

  extension.use({ resolversConfig: { [resolverPath]: { auth: { scope: [resolverScope] } } } });

  builder.field(attributeName, { type, resolve, args, ...options });
};

const isPrivate = (contentType: Model) => (attributeName: string) => {
  return contentTypes.isPrivateAttribute(contentType, attributeName);
};

const isDisabled = (contentType: Schema.ContentType) => (attributeName: string) => {
  const extension = getGraphQLService('extension');

  return !extension.shadowCRUD(contentType.uid).field(attributeName).hasOutputEnabled();
};

type Options = {
  builder: any
  attributeName: string
  attribute: Schema.Attributes
  contentType: Model
  type?: string
  authorize?: (parent: any, args: any, context: any, info: GraphQLResolveInfo) => boolean | Promise<boolean>
  resolve?: (parent: any, args: any, context: any, info: GraphQLResolveInfo) => unknown
}

type OverrideFieldOptions<Type extends Object, Args = unknown, Context = ResolveContext, Info = GraphQLResolveInfo> = {
  contentTypeName: string,
  fieldName: keyof Type,
  type?: string
  authorize?: (parent: Type, args: Args, context: Context, info: Info) => boolean | Promise<boolean>
  resolve?: (parent: Type, args: Args, context: Context, info: Info) => unknown
  extensions?: {
    middlewares?: Middleware[]
  }
}

export const overrideField = <T extends Object>(
  t: any,
  overrideOptions: OverrideFieldOptions<T>,
) => {
  const utils = getGraphQLService('utils');
  const { contentTypeName, fieldName } = overrideOptions;

  const {
    isStrapiScalar,
    isComponent,
    isDynamicZone,
    isEnumeration,
    isMedia,
    isMorphRelation,
    isRelation,
  } = utils.attributes;

  const contentType = strapi.contentTypes[contentTypeName as keyof typeof strapi.contentTypes]
  const { attributes } = contentType

  // @ts-ignore
  const attribute = attributes[fieldName]

  if (isPrivate(contentType)(attribute)) {
    return
  }

  if (isDisabled(contentType)(attribute)) {
    return
  }

  // We create a copy of the builder (t) to apply custom
  // rules only on the current attribute (eg: nonNull, list, ...)
  let builder = t;

  if (attribute.required) {
    builder = builder.nonNull;
  }

  const options = {
    builder,
    attributeName: fieldName,
    attribute,
    contentType,
    ...overrideOptions,
  };

  // Enums
  if (isEnumeration(attribute)) {
    // @ts-ignore
    addEnumAttribute(options);
  }

  // Scalars
  else if (isStrapiScalar(attribute)) {
    // @ts-ignore
    addScalarAttribute(options);
  }

  // Components
  else if (isComponent(attribute)) {
    // @ts-ignore
    addComponentAttribute(options);
  }

  // Dynamic Zones
  else if (isDynamicZone(attribute)) {
    // @ts-ignore
    addDynamicZoneAttribute(options);
  }

  // Media
  else if (isMedia(attribute)) {
    // @ts-ignore
    addMediaAttribute(options);
  }

  // Polymorphic Relations
  else if (isMorphRelation(attribute)) {
    // @ts-ignore
    addPolymorphicRelationalAttribute(options);
  }

  // Regular Relations
  else if (isRelation(attribute)) {
    // @ts-ignore
    addRegularRelationalAttribute(options);
  }
};
