/**
 * Easily resolve a collection of entities
 *
 * Based on node_modules/@strapi/plugin-graphql/server/services/builders/resolvers/association.js
 */

import { type ResolveArgs } from './types';

/**
 * Create a resolver for a collection of entities
 * Returns a resolver that resolves the entities based on the content type UID
 *
 * @param contentType The content type UID of the entities
 */
export const createEntityCollectionResolver = <ContentType extends Object>(
  contentType: string
) => {
  const { transformArgs } = strapi.plugin('graphql').service('builders').utils;
  const { toEntityResponseCollection } = strapi.plugin('graphql').service('format').returnTypes;
  const { buildQueriesResolvers } = strapi.plugin('graphql').service('builders').get('content-api');

  const { find } = buildQueriesResolvers({
    contentType: strapi.contentTypes[contentType as keyof typeof strapi.contentTypes]
  });

  return async (
    resolveArgs: ResolveArgs<ContentType>
  ): Promise<EntityResponseCollection<ContentType>> => {
    const transformedArgs = transformArgs(resolveArgs.args, {
      contentType: strapi.contentTypes[contentType as keyof typeof strapi.contentTypes],
      usePagination: true
    });

    const items = await find(resolveArgs.parent, transformedArgs, resolveArgs.context);

    return toEntityResponseCollection(items, {
      args: transformedArgs,
      resourceUID: contentType
    });
  };
};


/**
 * Create a resolver for a collection of entities related to a parent entity
 * Returns a resolver that resolves the related entities based on the relation field name
 *
 * @param contentType The content type UID of the related entities
 */
export function createEntityRelationCollectionResolver<Parent extends object>(contentType: string) {
  return async <K extends keyof Parent>(
    relationFieldName: K,
    resolveArgs: ResolveArgs<Parent>
  ): Promise<EntityResponseCollection<ExtractRelation<Parent, K>>> => {
    const { buildAssociationResolver } = strapi
      .plugin('graphql')
      .service('builders')
      .get('content-api');

    const find = buildAssociationResolver({
      contentTypeUID: contentType,
      attributeName: relationFieldName as string
    });

    return find(resolveArgs.parent, resolveArgs.args, resolveArgs.context) as Promise<
      EntityResponseCollection<ExtractRelation<Parent, K>>
    >;
  };
}

type EntityResponseCollection<T extends Object> = {
  nodes: T[]
  info: {
    args: Record<string, any>
    resourceUID: string
  }
}

// Helper type to extract Relation from Parent and K
type ExtractRelation<Parent, K extends keyof Parent> =
  Parent[K] extends Array<infer U>
    ? U extends object
      ? U
      : never
    : Parent[K] extends object
      ? Parent[K]
      : never;

export default function toEntityResponseCollection<T extends Object>(
  items: T[],
  { args, resourceUID }: { args: Record<string, any>; resourceUID: string }
): EntityResponseCollection<T> {
  const { toEntityResponseCollection } = strapi.plugin('graphql').service('format').returnTypes;

  return toEntityResponseCollection(items, {
    args,
    resourceUID
  });
}
