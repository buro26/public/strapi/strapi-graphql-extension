import register from './register';
import bootstrap from './bootstrap';
import destroy from './destroy';
import config from './config';
import contentTypes from './content-types';
import controllers from './controllers';
import routes from './routes';
import middlewares from './middlewares';
import policies from './policies';
import services from './services';
import type { Strapi } from '@strapi/strapi';
import type { GraphQLExtension } from './types';
import type { Middleware, ResolveContext } from './utils';
import type { GraphQLResolveInfo } from 'graphql';
import type { GraphQLFieldConfig } from 'graphql/type';

export default {
  register,
  bootstrap,
  destroy,
  config,
  controllers,
  routes,
  services,
  contentTypes,
  policies,
  middlewares
};

export * from './types';
export * from './utils';

export const createGraphQLExtension = (factory: (strapi: Strapi) => GraphQLExtension): GraphQLExtension => factory(strapi);

declare module '@strapi/strapi' {
  interface Strapi {
    createGraphQLExtension: (factory: (strapi: Strapi) => GraphQLExtension) => GraphQLExtension;
  }
}

/**
 * Extend Nexus's field definition to include a middleware array.
 */
declare module 'nexus/dist/core' {

  /**
   * Extends Nexus's OutputFieldConfig to include a `middleware` property.
   */
  interface NexusOutputFieldConfig<TypeName extends string, FieldName extends string> {

    extensions?: {
      middlewares?: Middleware[];
    } & GraphQLFieldConfig<any, any>['extensions']

    /**
     * A function that determines if the current user is authorized to access this field.
     *
     * @param parent The parent object.
     * @param args The arguments passed to the field.
     * @param context The context object.
     * @param info The GraphQLResolveInfo object.
     */
    authorize?: <Parent extends Object, Args extends Object, Context = ResolveContext>(parent: Parent, args: Args, context: Context, info: GraphQLResolveInfo) => boolean | Promise<boolean>;
  }

  interface GraphQLFieldExtensions<_TSource,_TContext,_TArgs> {
    /**
     * An array of middleware functions to apply to this field.
     */
    middleware?: Middleware[];
  }

}
