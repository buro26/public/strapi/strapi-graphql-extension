import type { Strapi } from '@strapi/strapi';
import type { GraphQLExtension } from './types';
import { fieldAuthorizePlugin } from 'nexus';
import { nexusMiddlewarePlugin } from './utils';

export default async ({ strapi }: { strapi: Strapi }) => {
  const fileService = strapi
    .plugin('burotwosix-graphql-plugin')
    .service('fileService');
  const graphQlExtensionService = strapi
    .plugin('graphql')
    .service('extension');

  const extensions: GraphQLExtension[] = [
    ...await fileService.getAllFilesInGraphQlDirectory(strapi.dirs.dist.src)
  ];

  graphQlExtensionService.use(() => ({
    plugins: [
      fieldAuthorizePlugin(),
      nexusMiddlewarePlugin(),
    ]
  }));

  for (const extension of extensions) {
    extension?.init && extension.init();

    graphQlExtensionService.use(() => ({
      typeDefs: extension?.typeDefs?.() ?? '',
      types: [
        ...(extension?.getTypes?.() ?? []),
        ...(extension?.types?.() ?? []),
      ],
      plugins: extension?.plugins?.() ?? [],
      resolvers: extension?.resolvers?.() ?? { Query: {}, Mutation: {} },
      resolversConfig: {
        ...extension?.getResolverConfig?.() ?? {},
        ...extension?.resolversConfig?.() ?? {},
      },
    }));
  }

  // @ts-ignore
  strapi.createGraphQLExtension = (factory: (strapi: Strapi) => GraphQLExtension): GraphQLExtension => factory(strapi);
}
