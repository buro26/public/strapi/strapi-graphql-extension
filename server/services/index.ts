import fileService from './file-service';
import graphQlService from './graphql-service';

export default {
  fileService,
  graphQlService,
}
