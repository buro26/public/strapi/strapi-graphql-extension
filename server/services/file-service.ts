import type { Strapi } from '@strapi/strapi';
import { promises as fsPromises } from 'fs';
import * as path from 'path';

import type { GraphQLExtension } from '../types';

export default ({ strapi }: { strapi: Strapi }) => ({

  /**
   * Fetch all the files in the graphql directories within the given directory path
   * Recursively searches for files in the graphql directory
   * Returns an array of GraphQLExtension objects
   *
   * @param directoryPath
   * @param collectedExtensions
   */
  async getAllFilesInGraphQlDirectory(directoryPath: string, collectedExtensions: GraphQLExtension[] = []): Promise<GraphQLExtension[]> {
    const fileEntries = await fsPromises.readdir(directoryPath);

    // Create an array of promises for all the files
    const filePromises = fileEntries.map(async (file) => {
      const filePath = path.join(directoryPath, file);
      const fileStats = await fsPromises.stat(filePath);

      if (fileStats.isDirectory()) {
        // Recursively get files from the directory
        return this.getAllFilesInGraphQlDirectory(filePath, collectedExtensions);
      }

      const folderName = 'graphql';
      if (
        !directoryPath.endsWith(path.sep + folderName) &&
        !directoryPath.includes(path.sep + folderName + path.sep)
      ) {
        return;
      }

      const graphqlModule = require(filePath.replace('.js', ''));

      if (!graphqlModule) {
        return;
      }

      collectedExtensions.push({
        ...graphqlModule,
        ...(graphqlModule.default || {})
      });
    });

    // Wait for all the promises to resolve
    await Promise.all(filePromises);

    return collectedExtensions;
  }

});
