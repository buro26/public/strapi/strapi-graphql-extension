import type { ResolveContext } from './utils';
import type { GraphQLResolveInfo } from 'graphql';
import type { Strapi } from '@strapi/strapi';


export interface GraphQLExtension {

  /**
   * Called when the extension is initialized, use this to perform any initialization logic
   * E.g. disabled CRUDs
   * @see https://docs.strapi.io/developer-docs/latest/plugins/graphql.html#disabling-operations-in-the-shadow-crud
   */
  init?: () => void

  /**
   * Return the type definitions for the extension as GraphQL SDL
   */
  typeDefs?: () => string

  /**
   * Return array the types for the extension
   * Use Nexus to define types
   *
   * @deprecated Use types() instead
   */
  getTypes?: () => any[]

  /**
   * Return array the types for the extension
   * Use Nexus to define types
   */
  types?: () => any[]

  /**
   * Return optional array of nexus plugins
   *
   * @see https://nexusjs.org/docs/plugins
   */
  plugins?: () => any[]

  /**
   * Return resolvers for the extension
   * Return an object with Query and Mutation resolvers
   */
  resolvers?: () => {
    Query?: Record<string, ResolverFunction>
    Mutation?: Record<string, ResolverFunction>
  },

  /**
   * Return resolver config for the extension
   * This is used to configure middlewares and other resolver options
   * @see https://docs.strapi.io/developer-docs/latest/plugins/graphql.html#extending-the-schema
   *
   * @deprecated Use resolversConfig() instead
   */
  getResolverConfig?: () => any

  /**
   * Return resolver config for the extension
   * This is used to configure middlewares and other resolver options
   * @see https://docs.strapi.io/developer-docs/latest/plugins/graphql.html#extending-the-schema
   */
  resolversConfig?: () => Record<string, ResolverConfigOption>
}

type ResolverConfigOption = {
  auth?: boolean | undefined
  policies?: string[] | PolicyFunction[]
  middlewares?: MiddlewareFunction[]
}

type PolicyFunctionContext = {
  parent: any
  args: any
  context: ResolveContext
  info: GraphQLResolveInfo
  state: ResolveContext['state']
  http: any
}

type PolicyFunctionOptions = {
  strapi: Strapi
}

type PolicyFunction = (context: PolicyFunctionContext, config: any, opts: PolicyFunctionOptions) => boolean | Promise<boolean>

type MiddlewareFunction = (next: MiddlewareNextFunction, parent: any, args: any, context: ResolveContext, info: GraphQLResolveInfo) => any | Promise<any>
type MiddlewareNextFunction = (parent: any, args: any, context: ResolveContext, info: GraphQLResolveInfo) => Promise<any>

type ResolverFunction = (parent: any, args: any, context: ResolveContext, info: GraphQLResolveInfo) => any | Promise<any>
